document.addEventListener('DOMContentLoaded', init);
let movePlayer = true;

function init() {
    createGameBoard();
    main();
    dragAndDrop();
    let btnReset = document.getElementById('reset');
    btnReset.addEventListener('click', resetF);
}

function createOneTile() {
    const board = document.querySelector('div.container');
    let tile = document.createElement('div');
    tile.classList.add('tile');
    board.prepend(tile);
}

function createGameBoard() {
    const COUNT_TILE = 9;
    for (let i = 0; i < COUNT_TILE; i++) {
        createOneTile();
    }
}

function putSign(tile) {
    if (!tile.classList.contains('act')) {
        if (movePlayer) {
            addX(tile);
        } else {
            addO(tile);
        }
    }

    let tileNotActiveCollection = document.querySelectorAll('.tile:not(.act)');
    let winner = checkBoard();
    if (winner !== '') {
        showWinner(winner);
    } else if (tileNotActiveCollection.length === 0) {
        showLoosed();
    }
}

function main() {
    const board = document.querySelector('div.container');
    board.onclick = function (event) {
        putSign(event.target);
    };
    document.addEventListener('keydown', function (key) {
        let tileCollection = document.getElementsByClassName('tile');
        let code = key.code;
        let isTileHasActiveArr = [];
        for (let i = 0; i < tileCollection.length; i++) {
            isTileHasActiveArr.push(tileCollection[i].classList.contains('active'));
        }
        let indexActive = isTileHasActiveArr.indexOf(true);
        if (code === 'ArrowLeft') {
            if (indexActive === -1) {
                tileCollection[0].classList.add('active');
            } else if (indexActive === 0) {
                tileCollection[0].classList.remove('active');
                tileCollection[tileCollection.length - 1].classList.add('active');
            } else {
                tileCollection[indexActive].classList.remove('active');
                tileCollection[indexActive - 1].classList.add('active');
            }
        } else if (code === 'ArrowRight') {
            if (indexActive === -1) {
                tileCollection[0].classList.add('active');
            } else if (indexActive === isTileHasActiveArr.length - 1) {
                tileCollection[isTileHasActiveArr.length - 1].classList.remove('active');
                tileCollection[0].classList.add('active');
            } else {
                tileCollection[indexActive].classList.remove('active');
                tileCollection[indexActive + 1].classList.add('active');
            }
        } else if (code === 'Enter') {
            putSign(tileCollection[indexActive]);
        }
    });
}

function checkBoard() {
    let tileCollection = document.getElementsByClassName('tile');
    let XOArray = [];
    let winner = '';

    for (let i = 0; i < tileCollection.length; i++) {
        XOArray.push(tileCollection[i].innerHTML);
    }

    if (XOArray[0] === 'x' && XOArray[1] === 'x' && XOArray[2] === 'x' ||
        XOArray[3] === 'x' && XOArray[4] === 'x' && XOArray[5] === 'x' ||
        XOArray[6] === 'x' && XOArray[7] === 'x' && XOArray[8] === 'x' ||
        XOArray[0] === 'x' && XOArray[3] === 'x' && XOArray[6] === 'x' ||
        XOArray[1] === 'x' && XOArray[4] === 'x' && XOArray[7] === 'x' ||
        XOArray[2] === 'x' && XOArray[5] === 'x' && XOArray[8] === 'x' ||
        XOArray[0] === 'x' && XOArray[4] === 'x' && XOArray[8] === 'x' ||
        XOArray[6] === 'x' && XOArray[4] === 'x' && XOArray[2] === 'x') {
        winner = 'X';
    } else if (XOArray[0] === 'o' && XOArray[1] === 'o' && XOArray[2] === 'o' ||
        XOArray[3] === 'o' && XOArray[4] === 'o' && XOArray[5] === 'o' ||
        XOArray[6] === 'o' && XOArray[7] === 'o' && XOArray[8] === 'o' ||
        XOArray[0] === 'o' && XOArray[3] === 'o' && XOArray[6] === 'o' ||
        XOArray[1] === 'o' && XOArray[4] === 'o' && XOArray[7] === 'o' ||
        XOArray[2] === 'o' && XOArray[5] === 'o' && XOArray[8] === 'o' ||
        XOArray[0] === 'o' && XOArray[4] === 'o' && XOArray[8] === 'o' ||
        XOArray[6] === 'o' && XOArray[4] === 'o' && XOArray[2] === 'o') {
        winner = 'O';
    }
    return winner;
}

function addX(that) {
    const PLAYER_PLACE = document.querySelector('span.display-player');
    if (that.innerHTML === '') {
        that.classList.add('act');
        that.classList.add('playerX');
        that.innerHTML = 'x';
    }

    movePlayer = !movePlayer;
    PLAYER_PLACE.classList.remove('playerX');
    PLAYER_PLACE.classList.add('playerO');
    PLAYER_PLACE.innerHTML = 'O';
}

function addO(that) {
    const PLAYER_PLACE = document.querySelector('span.display-player');
    if (that.innerHTML === '') {
        that.classList.add('act');
        that.classList.add('playerO');
        that.innerHTML = 'o';
    }

    movePlayer = !movePlayer;
    PLAYER_PLACE.classList.remove('playerO');
    PLAYER_PLACE.classList.add('playerX');
    PLAYER_PLACE.innerHTML = 'X';
}

function showWinner(winner) {
    const DiSPLAY_WINNER_PLACE = document.querySelector('div.announcer');
    DiSPLAY_WINNER_PLACE.innerHTML = 'Player ';
    let span = document.createElement('span');
    span.innerHTML = `${winner} won`;
    span.classList.add(`player${winner}`);
    DiSPLAY_WINNER_PLACE.append(span);
    DiSPLAY_WINNER_PLACE.classList.remove('hide');
}

function showLoosed() {
    const DiSPLAY_WINNER_PLACE = document.querySelector('div.announcer');
    DiSPLAY_WINNER_PLACE.innerHTML = 'Nobody won';
    DiSPLAY_WINNER_PLACE.classList.remove('hide');
}

function resetF() {
    location.reload();
}

function dragAndDrop() {
    const items = document.getElementsByClassName('avatar-icon');
    for (let i = 0; i < items.length; i++) {
        items[i].addEventListener('dragstart', drag);
        items[i].setAttribute('draggable', 'true');
    }
    const avatarContainer = document.querySelectorAll('.avatar-container');
    avatarContainer.forEach(element => {
        element.addEventListener('drop', drop);
        element.addEventListener('dragover', allowDrop);
    });
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData('text', ev.target.dataset.item);
}

function drop(ev) {
    if (!ev.currentTarget.hasChildNodes()) {
        ev.preventDefault();
        let data = ev.dataTransfer.getData('text');
        ev.target.appendChild(document.querySelector(`[data-item="${data}"]`));
    }
}
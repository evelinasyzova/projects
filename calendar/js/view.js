import { FIRST_MONTH, LAST_MONTH } from './model.js';
import generateCalendar from './controller.js';

let currDate = new Date();
let currMonth = { value: currDate.getMonth() };
let currYear = { value: currDate.getFullYear() };

document.querySelector('#prev-month').onclick = () => {
    if (currMonth.value === FIRST_MONTH) {
        currMonth.value = LAST_MONTH;
        --currYear.value;
    } else {
        --currMonth.value;
    }
    generateCalendar(currMonth.value, currYear.value);
}

document.querySelector('#next-month').onclick = () => {
    if (currMonth.value === LAST_MONTH) {
        currMonth.value = FIRST_MONTH;
        ++currYear.value;
    } else {
        ++currMonth.value;
    }
    generateCalendar(currMonth.value, currYear.value);
}

generateCalendar(currMonth.value, currYear.value);
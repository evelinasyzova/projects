
const FIRST_MONTH = 0;
const LAST_MONTH = 11;

const MONTH_NAMES = [
    'January', 'February', 'March', 'April',
    'May', 'June', 'July', 'August',
    'September', 'October', 'November', 'December'
];



export { FIRST_MONTH, LAST_MONTH, MONTH_NAMES};

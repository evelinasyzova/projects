import { MONTH_NAMES } from './model.js';

function daysInMonth(month, year) {
    return new Date(year, month + 1, 0).getDate();
}

export default function generateCalendar(month, year) {
    let calendar = document.querySelector('.calendar');
    let monthHolder = calendar.querySelector('#month-holder');

    let calendarDays = calendar.querySelector('.calendar-days');
    let calendarHeaderYear = calendar.querySelector('#year');
    let daysOfMonth = daysInMonth(month, year);

    calendarDays.innerHTML = '';

    let currDate = new Date();

    let currMonth = `${MONTH_NAMES[month]}`;
    monthHolder.innerHTML = currMonth;
    calendarHeaderYear.innerHTML = year;

    let firstDay = new Date(year, month, 1);

    for (let i = 0; i <= daysOfMonth + firstDay.getDay() - 1; i++) {
        let day = document.createElement('div');
        if (i >= firstDay.getDay()) {
            day.innerHTML = i - firstDay.getDay() + 1;
            if (i - firstDay.getDay() + 1 === currDate.getDate()
                && year === currDate.getFullYear()
                && month === currDate.getMonth()) {
                day.classList.add('curr-date');
            }
        }
        calendarDays.appendChild(day);
    }
}
import { dictionary } from './dictionary.js';

let randomWord = getRandomWord();
let numberRow = 0;
addOnEditHendler();

function getRandomWord() {
    let randomNumber = Math.floor(Math.random() * dictionary.length);
    return dictionary[randomNumber];
}

function hasWordInDictionary(word) {
    return dictionary.includes(word);
}

function colorWord(expectedWord, row) {
    for (let i = 0; i < row.length; i++) {
        let index = expectedWord.indexOf(row[i].value.toLowerCase())
        if (index === -1) {
            row[i].style.backgroundColor = 'gray';
        } else if (index >= 0) {
            row[i].style.backgroundColor = 'yellow';
        }
        index = expectedWord.indexOf(row[i].value.toLowerCase(), i)
        if (index === i) {
            row[i].style.backgroundColor = 'green';
        }
    }
}

function getWord(numberRow) {
    let row = getRow(numberRow);
    let word = '';
    for (let i = 0; i < row.length; i++) {
        word += row[i].value;
    }
    return word;
}

function getRow(numberRow) {
    return document.getElementsByClassName('row')[numberRow].getElementsByTagName('input');
}

window.reset = function reset() {
    let inputs = document.getElementsByTagName('input');
    for (let i = 0; i < inputs.length; i++) {
        inputs[i].value = '';
        inputs[i].style.backgroundColor = 'white';
    }
    randomWord = getRandomWord();
    numberRow = 0;
}

function disabledCheckBtn() {
    let word = getWord(numberRow);
    let btnCheck = document.querySelector('.btnCheck');
    if (word.length === 5) {
        btnCheck.removeAttribute('disabled');
    } else {
        btnCheck.setAttribute('disabled', true);
    }
}

function addOnEditHendler() {
    let inputs = document.getElementsByTagName('input');
    for (let i = 0; i < inputs.length; i++) {
        inputs[i].addEventListener('input', disabledCheckBtn);
    }
}

window.check = function check() {
    if (hasWordInDictionary(getWord(numberRow))) {
        colorWord(randomWord, getRow(numberRow));
        if (getWord(numberRow) === randomWord) {
            alert('Congratulations! You won.');
        } else if (numberRow > 4) {
            alert('Game over.');
            window.reset();
        } else {
            numberRow++;
        }
    } else {
        alert('Not in word list');
    }
}


const COUNT_MS = 100;
let btnLoadByJS = document.querySelector('.load-by-js');
btnLoadByJS.addEventListener('click', listenerLoadByJs);

let btnLoadByFetch = document.querySelector('.load-by-fetch');
btnLoadByFetch.addEventListener('click', listenerLoadByFetch);

let isloadByJS = true;
function listenerLoadByJs() {
    if (isloadByJS) {
        const url = 'https://jsonplaceholder.typicode.com/users';
        const xhr = new XMLHttpRequest();
        xhr.open('GET', url);
        xhr.onload = () => {
            const parsed = JSON.parse(xhr.response);
            for (let index = 0; index < parsed.length; index++) {
                const name = parsed[index].name;
                let divUserListByJS = document.querySelector('.users-list-by-js');
                let listByJS = document.createElement('div');
                listByJS.classList.add('list-item');
                divUserListByJS.append(listByJS);
                let headerListByJS = document.createElement('h2');
                headerListByJS.classList.add('header-by-js');
                headerListByJS.innerHTML = name;
                listByJS.append(headerListByJS);
            }
        }
        xhr.send();
        isloadByJS = false;
    }
}
let isloadByFetch = true;
function listenerLoadByFetch() {
    if (isloadByFetch) {
        const request = new Request('https://jsonplaceholder.typicode.com/users');
        fetch(request)
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw Error();
                }
            })
            .then(json => {
                for (let index = 0; index < json.length; index++) {
                    const name = json[index].name;
                    const id = json[index].id;
                    createListItemByFetch(name, id);
                }
            })
            .catch(() => {
                alert('Something went wrong');
            });
        isloadByFetch = false;
    }
}

function createListItemByFetch(name, id) {
    let divUserListByFetch = document.querySelector('.users-list-by-fetch');
    let listbyFetch = document.createElement('div');
    listbyFetch.classList.add('list-item');
    divUserListByFetch.append(listbyFetch);
    let headerListByFetch = document.createElement('h2');
    headerListByFetch.innerHTML = name;
    listbyFetch.append(headerListByFetch);
    let btnEditByFetch = document.createElement('button');
    btnEditByFetch.innerHTML = 'Edit';
    btnEditByFetch.classList.add('btn-edit');
    listbyFetch.append(btnEditByFetch);
    let btnDeleteByFetch = document.createElement('button');
    btnDeleteByFetch.innerHTML = 'Delete';
    btnDeleteByFetch.classList.add('btn-delete');
    listbyFetch.append(btnDeleteByFetch);
    btnDeleteByFetch.addEventListener('click', () => {
        showLoader(listbyFetch);
        fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
            method: 'DELETE'
        })
            .then(response => {
                if (response.ok) {
                    hideLoader();
                    setTimeout(() => {
                        alert(`User with id – ${id} was deleted`);
                    }, COUNT_MS);
                }
            })
    })
    let isEdit = true;
    btnEditByFetch.addEventListener('click', () => {
        let btnSaveByFetch = document.createElement('button');
        let inputByFetch = document.createElement('input');
        let divEditContainer = document.createElement('div');
        if (isEdit) {
            divEditContainer.classList.add('edit-container');
            listbyFetch.append(divEditContainer);
            inputByFetch.classList.add('input-by-fetch');
            divEditContainer.append(inputByFetch);
            btnSaveByFetch.classList.add('btn-by-fetch');
            btnSaveByFetch.innerHTML = 'Save';
            divEditContainer.append(btnSaveByFetch);
            isEdit = false;
        }
        btnSaveByFetch.addEventListener('click', () => {
            showLoader(divEditContainer);
            fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
                method: 'PUT',
                body: JSON.stringify({
                    name: inputByFetch.value
                }),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8'
                }
            })
                .then(response => {
                    if (response.ok) {
                        return response.json()
                    }
                }).then(json => {
                    hideLoader();
                    setTimeout(() => {
                        alert(`User with id – ${id} was updated. New name is ${json.name}`);
                    }, COUNT_MS);
                })
        })
    })
}

function showLoader(divEditContainer) {
    const loader = document.createElement('div');
    loader.setAttribute('id', 'loading');
    divEditContainer.append(loader);
    loader.classList.add('display');
}

function hideLoader() {
    const loader = document.getElementById('loading')
    loader.classList.remove('display');
    loader.remove();
}





